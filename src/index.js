import "./css/style.css";
// import Icon from "./icons/icon.png";
import "./styles.css";
//
// if (process.env.NODE_ENV !== "production") {
//   console.log("Looks like we are in development mode!");
// }
//
// async function getComponent() {
//   var element = document.createElement("div");
//   const { default: _ } = await import("lodash");
//
//   element.innerHTML = _.join(["Hello", "webpack"], " ");
//
//   return element;
// }
//
// getComponent().then(component => {
//   document.body.appendChild(component);
// });
//
// console.log("asd");

import _ from "lodash";

function component() {
  var element = document.createElement("div");
  var button = document.createElement("button");
  var br = document.createElement("br");

  button.innerHTML = "Click me and look at the console!";
  element.innerHTML = _.join(["Hello", "webpack"], " ");
  element.appendChild(br);
  element.appendChild(button);

  // Note that because a network request is involved, some indication
  // of loading would need to be shown in a production-level site/app.
  button.onclick = e =>
    import("./print").then(module => {
      var print = module.default;

      print();
    });

  return element;
}

document.body.appendChild(component());
